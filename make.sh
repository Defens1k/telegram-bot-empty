#!/bin/bash
PKGNAME=$(cat env/pkgname)
VERSION=1.$(cat env/version)
LOCAL_REP_COUNT=$(dpkg -l | grep local-rep | wc -l)

if [[  $LOCAL_REP_COUNT = "0"  ]]
then echo "install package local-rep, exec: git clone git@gitlab.com:Defens1k/local-rep.git"
else echo "local-rep pkg found"
fi

scripts/deb.sh
cd filesystem
dpkg-deb --build . ../$(echo $PKGNAME)_$(echo $VERSION)_all.deb
cp ../*.deb ../build
mv ../*.deb /usr/local/mydebs
